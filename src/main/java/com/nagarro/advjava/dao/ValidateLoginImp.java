package com.nagarro.advjava.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nagarro.advjava.entity.User;

@Repository
public class ValidateLoginImp implements ValidateLogin {
	
	@Autowired 
	private SessionFactory sessionFactory;

	@Override
	public boolean isUserValid(User user) {
		
		boolean isValid= false;
		
		if(user.getUsername().equals("sumit") && user.getPassword().equals("12345"))
			isValid = true;
		
		return isValid;
	}

}
