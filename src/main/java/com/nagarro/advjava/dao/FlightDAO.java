package com.nagarro.advjava.dao;

import java.util.List;

import com.nagarro.advjava.entity.Flight;

public interface FlightDAO {
	
	public List<Flight> getFlightList();

	public List<Flight> getSelectedFlightList(String fltDepLoc, String fltArrLoc, String fltDate, String fltClass,
			String sortOrder);

	
}
