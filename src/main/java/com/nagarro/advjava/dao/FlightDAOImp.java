package com.nagarro.advjava.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nagarro.advjava.entity.Flight;
import com.nagarro.advjava.three.SortbyDuration;
import com.nagarro.advjava.three.SortbyFare;

@Repository
public class FlightDAOImp implements FlightDAO {
	
	@Autowired 
	private SessionFactory sessionFactory;
	
	public List<Flight> getFlightList() {
		
		Session currSession = sessionFactory.getCurrentSession();
		
		Query<Flight> fltQuery = currSession.createQuery("from Flight", Flight.class);
		
		List<Flight> flights = fltQuery.getResultList();
		
		return flights;
	}
	
	
	public List<Flight> getSelectedFlightList(String fltDepLoc, String fltArrLoc, String fltDate, String fltClass,
			String sortOrder) {
		
		
		Session currSession = sessionFactory.getCurrentSession();
		
		Query<Flight> fltQuery = currSession.createQuery("from Flight where fltDepLoc = '"+fltDepLoc+"' AND fltArrLoc= '"+fltArrLoc+"'", Flight.class);
		
		List<Flight> flightList = (ArrayList<Flight>) fltQuery.getResultList();
		
		flightList = flightList.stream().filter(flight -> flight.isAvailable(fltDate, fltClass))
				.collect(Collectors.toList()); 
		
		if(sortOrder.equals("F")) {
			Collections.sort(flightList, new SortbyFare());
		} else {
			Collections.sort(flightList, new SortbyFare()
                    .thenComparing(new SortbyDuration()));
		}
		
		return flightList;
	}

}
