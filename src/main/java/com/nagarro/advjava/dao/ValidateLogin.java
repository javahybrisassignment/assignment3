package com.nagarro.advjava.dao;

import com.nagarro.advjava.entity.User;

public interface ValidateLogin {
	
	public boolean isUserValid(User user);
	
}
