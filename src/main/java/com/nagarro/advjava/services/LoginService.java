package com.nagarro.advjava.services;

import com.nagarro.advjava.entity.User;

public interface LoginService {
	
	public boolean isUserValid(User user);
	
}
