package com.nagarro.advjava.services;

import java.util.List;

import com.nagarro.advjava.entity.Flight;

public interface FlightService {
	
	public List<Flight> getFlightList();

	public List<Flight> getSelectedFlightList(String fltDepLoc, String fltArrLoc, String fltDate, String fltClass,
			String sortOrder);
	
}
