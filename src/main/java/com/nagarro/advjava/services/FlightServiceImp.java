package com.nagarro.advjava.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.advjava.dao.FlightDAO;
import com.nagarro.advjava.entity.Flight;

@Service
@Transactional
public class FlightServiceImp implements FlightService {
	
	@Autowired
	private FlightDAO flightDAO;

	public List<Flight> getFlightList() {
		
		return flightDAO.getFlightList();
	}

	public List<Flight> getSelectedFlightList(String fltDepLoc, String fltArrLoc, String fltDate, String fltClass,
			String sortOrder) {
		
		List<Flight> flightList = flightDAO.getSelectedFlightList( fltDepLoc, fltArrLoc, fltDate, fltClass, sortOrder);
		
		return flightList;
	}

}
