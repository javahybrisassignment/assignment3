package com.nagarro.advjava.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.advjava.dao.ValidateLogin;
import com.nagarro.advjava.entity.User;

@Service
@Transactional
public class LoginServiceImp implements LoginService {

	@Autowired
	private ValidateLogin validateDOA;
	
	@Override
	public boolean isUserValid(User user) {
		
		boolean isValid = validateDOA.isUserValid(user);
		
		return isValid;
	}

}
