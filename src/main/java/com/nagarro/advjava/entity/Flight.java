package com.nagarro.advjava.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="flight")
public class Flight implements Serializable {
	
	@Id
	@Column(name="FLIGHT_NO")
	private String fltNo;
	
	@Id
	@Column(name="DEP_LOC")
	private String fltDepLoc;
	
	@Id
	@Column(name="ARR_LOC")
	private String fltArrLoc;
	
	@Id
	@Column(name="VALID_TILL")
	private Date fltValidTill;
	
	@Id
	@Column(name="FLIGHT_TIME")
	private int fltTime;
	
	@Column(name="FLIGHT_DUR")
	private float fltDur;
	
	@Column(name="FARE")
	private int fltFare;
	
	@Column(name="SEAT_AVAILABILITY")
	private String fltSeatAvail;
	
	@Column(name="CLASS")
	private String fltClass;
	
	public String getFltNo() {
		return fltNo;
	}

	public void setFltNo(String fltNo) {
		this.fltNo = fltNo;
	}

	public String getFltDepLoc() {
		return fltDepLoc;
	}

	public void setFltDepLoc(String fltDepLoc) {
		this.fltDepLoc = fltDepLoc;
	}

	public String getFltArrLoc() {
		return fltArrLoc;
	}

	public void setFltArrLoc(String fltArrLoc) {
		this.fltArrLoc = fltArrLoc;
	}

	public Date getFltValidTill() {
		return fltValidTill;
	}

	public void setFltValidTill(Date fltValidTill) {
		this.fltValidTill = fltValidTill;
	}

	public int getFltTime() {
		return fltTime;
	}

	public void setFltTime(int fltTime) {
		this.fltTime = fltTime;
	}

	public float getFltDur() {
		return fltDur;
	}

	public void setFltDur(long fltDur) {
		this.fltDur = fltDur;
	}

	public int getFltFare() {
		return fltFare;
	}

	public void setFltFare(int fltFare) {
		this.fltFare = fltFare;
	}

	public String getFltSeatAvail() {
		return fltSeatAvail;
	}

	public void setFltSeatAvail(String fltSeatAvail) {
		this.fltSeatAvail = fltSeatAvail;
	}

	public String getFltClass() {
		return fltClass;
	}

	public void setFltClass(String fltClass) {
		this.fltClass = fltClass;
	}
	
	public boolean isAvailable(String fltValid, String fltClass) {
		
		boolean flag = false;
		Date dt = null;
		try {
			dt = new SimpleDateFormat("yyyy-MM-dd").parse(fltValid);
		} catch (ParseException e) {
			e.printStackTrace();
		}
			
		if(this.fltClass.contains(fltClass) && (this.fltValidTill.compareTo(dt)>=0) && this.fltSeatAvail.equalsIgnoreCase("y"))
			flag = true;
		
		return flag;		
	}
}
 
