package com.nagarro.advjava.three;

import java.util.Comparator;

import com.nagarro.advjava.entity.Flight;


public class SortbyFare implements Comparator<Flight> 
{ 
    // Used for sorting in ascending order 
    public int compare(Flight a, Flight b) 
    { 
        return a.getFltFare() - b.getFltFare(); 
    } 
} 