package com.nagarro.advjava.controller;

import java.io.File;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.nagarro.advjava.entity.User;



public class Database {
public static void saveUser(String username, String password) {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(User.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		try {
		
					
			User u = new User(username, password);
					
			session.save(u);
					
			session.getTransaction().commit();
			
			//
					
		} finally {
//			session.flush();
//			session.close();
//			factory.close();
		}		
	}	
}
