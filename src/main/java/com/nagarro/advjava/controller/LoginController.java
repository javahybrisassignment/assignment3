package com.nagarro.advjava.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.nagarro.advjava.entity.User;
import com.nagarro.advjava.services.LoginService;

@Controller
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	
	@RequestMapping("/")
	public String showPage() {
		return "main-menu";
	}
	
	@RequestMapping("/login")
	public String showForm(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("errormsg", "");
		return "login-page";
	}
	
	@RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
	  public String loginProcess(HttpServletRequest request, HttpServletResponse response,
	  @ModelAttribute("user") User user, Model model) {
		  
	    boolean isUserValid = loginService.isUserValid(user);
	    
	    String screenName = "login-page";
	    
	    if (isUserValid == true) {
		    model.addAttribute("isValid", true);
		    screenName = "search-flights";
		    
	    } else {
	    	model.addAttribute("isValid", false);
	    	model.addAttribute("errormsg", "Please enter valid credentials");
	    }
	    return screenName;
	  }
}
