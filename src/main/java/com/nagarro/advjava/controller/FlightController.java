package com.nagarro.advjava.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.nagarro.advjava.dao.FlightDAO;
import com.nagarro.advjava.entity.Flight;
import com.nagarro.advjava.entity.User;
import com.nagarro.advjava.services.FlightService;
import com.nagarro.advjava.services.LoginService;
import com.nagarro.advjava.services.LoginServiceImp;

@Controller
@RequestMapping("/flights")
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	@Autowired
	private LoginService loginService;
	
	@RequestMapping("/showSearch")
	public String searchFlights(Model fModel) {
		
		List<Flight> flightsList = flightService.getFlightList();
		
		fModel.addAttribute("flightsList", flightsList);
		
		return "search-flights";		
	}
	
	@RequestMapping("/show")
	public String listFlights(Model fModel,
			@RequestParam(name = "fltDepLoc") String fltDepLoc,
			@RequestParam(name = "fltArrLoc") String fltArrLoc,
			@RequestParam(name = "fltDate") String fltDate,
			@RequestParam(name = "fltClass") String fltClass,
			@RequestParam(name = "sortOrder") String sortOrder) {
		
		List<Flight> flightsList = flightService.getSelectedFlightList(fltDepLoc, fltArrLoc, fltDate, fltClass, sortOrder);
		
		fModel.addAttribute("flightsList", flightsList);
		
		return "list-flights";		
	}
	
}
