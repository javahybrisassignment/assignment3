<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<script  src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/imggallery.css">
<title>Insert title here</title>
</head>
<body>
	<div id="wrapper">
		<div class="col-md-11 text-center">
			<h2>Flights</h2>
				<div class="flight-list">
					<table class="table">
					  <thead class="thead-light">
					    <tr>
					      <th scope="col">Flight No.</th>
					      <th scope="col">Departure</th>
					      <th scope="col">Arrival</th>
					      <th scope="col">Date</th>
					      <th scope="col">Time</th>
					      <th scope="col">Duration</th>
					      <th scope="col">Fare</th>
					      <th scope="col">Class</th>
					    </tr>
					  </thead>
					  <tbody>
					  <c:forEach var="flt" items="${flightsList}">
			          	<tr>
					      <td>${flt.fltNo}</td>
					      <td>${flt.fltDepLoc}</td>
					      <td>${flt.fltArrLoc}</td>					      
					      <td><fmt:formatDate value="${flt.fltValidTill}" pattern="dd-MM-yyyy" /></td>
					      <td>${flt.fltTime}</td>
					      <td>${flt.fltDur}</td>
					      <td>${flt.fltFare}</td>
					      <td>${flt.fltClass}</td>
					    </tr>	
					    
					    </c:forEach>	
					    				    
					  </tbody>
					</table>
				</div>
		</div>
		<div><a href="/AssignmentThree/flights/showSearch">Goto Search </a>|<a href="/AssignmentThree/login"> Logout </a></div>
	</div>
</body>
</html>