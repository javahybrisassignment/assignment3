<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Flight Search : Search Page</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
	<style type="text/css">
		.container {
			width: 400px;
		   	margin: 50px auto;
		}
		   .container{
		   	margin-bottom: 15px;
		    
		    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
		    padding: 30px;
		}
		.login-form h2 {
		    margin: 0 0 15px;
		}
		.form-control, .btn {
		    min-height: 38px;
		    border-radius: 2px;
		}
		.btn {        
		    font-size: 15px;
		    font-weight: bold;
		}
	
	</style>
</head>
<body>
	<div id="wrapper">
	<div class="container">
		<h2> Search Indian Flights </h2>
		<hr/>
		<div class="form-wrapper">
			<form action="flights/show" method="GET">
			
			    <div class="form-group col-md-6">
			      <label for="fltDepLoc">Departure Location</label>
			      <input type="text" class="form-control" name="fltDepLoc" id="fltDepLoc">
			    </div>
			    
			    <div class="form-group col-md-6">
			      <label for="inputPassword4">Arrival Location</label>
			      <input type="text" class="form-control" name="fltArrLoc" id="fltArrLoc">
			    </div>
			    
				  <div class="form-group ">
				    <label for="inputAddress">Date of Travel</label>
				    <input type="date" class="form-control" name="fltDate" id="fltDate">
				  </div>
			  
			  	<div>
			  		<h3>Choose class type of flight</h3>	 		
		  			<div class="form-check">
					  <input class="form-check-input" type="radio" name="fltClass" id="bClass" value="E" checked>
					  <label class="form-check-label" for="bClass">
					    Economy Class
					  </label>
					</div>
					<div class="form-check">
					  <input class="form-check-input" type="radio" name="fltClass" id="eClass" value="B">
					  <label class="form-check-label" for="eClass">
					    Business Class
					  </label>
					</div> 		  	
			  	</div>
			  	
			  	
			  	<div>
			  		<h3>Please select sorting oder</h3>	 		
		  			<div class="form-check">
					  <input class="form-check-input" type="radio" name="sortOrder" id="orderF" value="F" checked>
					  <label class="form-check-label" for="orderF">
					    Sort Flights Based On fares
					  </label>
					</div>
					<div class="form-check">
					  <input class="form-check-input" type="radio" name="sortOrder" id="orderFD" value="FD">
					  <label class="form-check-label" for="orderFD">
					    Sort Flights Based On fares and flight Duration
					  </label>
					</div> 		  	
			  	</div>	
			  	<br>	
			  	<div>
			  		<button type="submit" class="btn btn-primary">Search Flights</button>
			  	</div>
			  					  		
			  
			</form>
		</div>
		<div><a href="/AssignmentThree/login"> Logout </a></div>
	</div>
	
	</div>
</body>
</html>